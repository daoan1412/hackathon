import { Router } from 'express'
import { posts } from '../database/Posts'
import { posix } from 'path';

var multer = require('multer')

const router = Router()

const uploadsAudioFolder = 'uploads/'

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, uploadsAudioFolder)
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '.' + Math.random() + '.' + file.originalname.split('.').reverse()[0])
    }
})
let upload = multer({
    storage: storage,
})

router.post('/photos/upload', upload.single('photo'), function (req, res, next) {
    if (req.file) {
        res.status(200).json({ 'filename': req.file.filename })
    } else {
        res.status(400).json({ 'result': 'Tải lên không thành công' })
    }
})

router.post('/post/add', async (req, res, next) => {
    const { name, description, location, photos, topography } = req.body
    let result = await posts.add(name, description, location, photos, topography)
    if (result) {
        res.json(result)
    } else {
        res.status(400).json(result)
    }
})

router.get('/post/showAllByNewest', async (req, res, next) => {
    let { perPage, currentPage } = req.query
    res.json(await posts.filter({}, perPage, currentPage, { _id: -1 }))
})

router.get('/post/:id', async (req, res, next) => {
    let id = req.params.id
    res.json(await posts.filter({ '_id': id }, 1, 1, { "_id": -1 }))
})

router.get('/post/showAllByTopography', async (req, res, next) => {
    let { perPage, currentPage, topography } = req.query
    res.json(await posts.filter({ 'post.topography': topography }, perPage, currentPage, { "_id": -1 }))
})

router.get('/post/showAllByRating', async (req, res, next) => {
    let { perPage, currentPage, topography } = req.query
    res.json(await posts.filter({}, perPage, currentPage, {
        'post.rating': -1
    }))
})

router.get('/post/find', async (req, res, next) => {
    let { text } = req.query
    res.json(await posts.findByText(text))
})

router.post('/post/count', async (req, res, next) => {
    res.json(await posts.count(req.body))
})

router.post('/post/addReview', async (req, res, next) => {
    let { id, rating, reviews } = req.body
    res.json(await posts.addReviews(id, rating, reviews))
})

export default router
