const Mongoose = require('mongoose')

Mongoose.Promise = global.Promise;
export const connectToDb = async () => {
  const uri = 'mongodb://hkt:123456@ds119738.mlab.com:19738/hkt';
  try {
    await Mongoose.connect(uri);
    console.log('Connected to mongo!!! :)');
  }
  catch (err) {
    console.error(err);
    process.exit(1);
  }
}