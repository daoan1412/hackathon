import mongoose from 'mongoose'
import { fail } from 'assert';
let Schema = mongoose.Schema;

class Posts {
    constructor() {
        this.configModel()
    }
    configModel() {
        let schema = new Schema({
            post:
                {
                    name: String,
                    description: String,
                    lat: Number,
                    lng: Number,
                    topography: {
                        type: String,
                        enum: ['nui', 'bien'],
                    },
                    reviews: Number,
                    rating: Number,
                    photos: Array
                }
        });
        schema.index({ '$**': 'text' });
        this.Post = mongoose.model('post', schema);
    }
    async add(name, description, location, photos, topography) {
        let post = new this.Post({
            post: {
                name,
                description,
                lat: location['lat'],
                lng: location['lng'],
                photos,
                topography,
            }
        })
        try {
            await post.save()
            return true;
        } catch (error) {
            console.log(error);
            return false;
        }
    }


    async filter(field, perPage, currentPage, sortBy) {
        try {
            return await this.Post.find(field)
                .limit(Number(perPage))
                .skip(perPage * (Number(currentPage) - 1))
                .sort(sortBy)
                .exec()
        } catch (error) {
            console.error(error);
            return false;
        }
    }

    async findByText(text) {
        try {
            return await this.Post.find({ $text: { $search: text } })
        } catch (error) {
            console.error(error);
            return false;
        }
    }

    async addReviews(id, rating, reviews) {
        try {
            await this.Post.update({ _id: id }, { $set: { 'post.reviews': reviews, 'post.rating': rating } });
            return await this.Post.findById(id);
        } catch (error) {
            console.log(error);
            return false
        }
    }

    async count(obj) {
        try {
            return await this.Post.count(obj);
        } catch (error) {
            console.log(error);
            return false
        }
    }

}
export const posts = new Posts()
